package it.unitn.url.analysis.document.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.MutableAggregationBuffer;
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class ConcatString extends UserDefinedAggregateFunction{

	/**
	 * 
	 */
	private StructType _inputDataType;
	private StructType _bufferSchema;
	private DataType _returnDataType;
	
	private static final long serialVersionUID = -3309850085051842291L;

	public  ConcatString() {
		List<StructField> inputFields = new ArrayList<StructField>();
		inputFields
				.add(DataTypes.createStructField("inputsTRING", DataTypes.StringType, true));
		_inputDataType = DataTypes.createStructType(inputFields);

		List<StructField> bufferFields = new ArrayList<StructField>();
		bufferFields.add(DataTypes.createStructField("bufferString", DataTypes.StringType, true));
		_bufferSchema = DataTypes.createStructType(bufferFields);

		_returnDataType = DataTypes.StringType;
	}
	@Override
	public StructType bufferSchema() {
	
		return _bufferSchema;
	}

	@Override
	public DataType dataType() {
	
		return _returnDataType;
	}

	@Override
	public boolean deterministic() {
		
		return true;
	}

	@Override
	public Object evaluate(Row arg0) {
		if (arg0.isNullAt(0)) {
			return null;
		} else {
			return arg0.getString(0);
		}
	}

	@Override
	public void initialize(MutableAggregationBuffer arg0) {
		arg0.update(0, new String());
		
	}

	@Override
	public StructType inputSchema() {
		// TODO Auto-generated method stub
		return _inputDataType;
	}

	@Override
	public void merge(MutableAggregationBuffer arg0, Row arg1) {
		if (!arg0.isNullAt(0)) {
			if (arg0.isNullAt(0)) {
				arg0.update(0, arg1.getString(0));
			} else {
				String oo = arg0.getString(0) + " " + arg1.getString(0);
				arg0.update(0, oo);
			}
		}
		
	}

	@Override
	public void update(MutableAggregationBuffer arg0, Row arg1) {
		if (!arg1.isNullAt(0)) {
			if (arg0.isNullAt(0)) {
				arg0.update(0, arg1.getString(0));
			} else {
				String oo = arg0.getString(0) + " " + arg1.getString(0);
				arg0.update(0, oo);
			}
		}
		
	}

}
