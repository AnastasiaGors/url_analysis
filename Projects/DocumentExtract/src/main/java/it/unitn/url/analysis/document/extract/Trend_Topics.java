package it.unitn.url.analysis.document.extract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.api.java.UDF3;
import org.apache.spark.sql.hive.HiveContext;
import org.apache.spark.sql.types.DataTypes;
import org.bson.BSONObject;

import com.mongodb.hadoop.MongoInputFormat;

import it.unitn.url.analysis.document.common.DocumentsSchema;
import scala.Tuple2;
public class Trend_Topics {
	final static int  slotTime = 5;
	final static int  HOUR = 60;
	static int  DAY = 3600;

	final static int slideWindows = 4;
	final static int halfSlideWindows=2;   
	final static int topicsToRank = 3;

	final static Long  slotTimeInSeconds = 60L * slotTime;
	final static Long  slotHourInSeconds = 60L * HOUR;
	final static String mongoConnInput = "mongodb://localhost:27017/local.newEntry";
	final static String mongoConnInputTopicsCellId = "mongodb://localhost:27017/local.topics_slotTime_idCell";
	final static String mongoConnInputTopicstLac= "mongodb://localhost:27017/local.topics_slotTime_lac";
	private static final Logger log = Logger.getLogger(Trend_Topics.class);

	public static String SlideWindonwExtract(int idSlot){
		String list = null;
		int slot = idSlot -1;
		int countSlot = slideWindows -1;
		while (countSlot > 0){    
			if (list == null) list = " "+ slot;
			else  list += ", "+ slot;

			slot -= 1;
			countSlot -= 1;	            
		}
		return list;
	}
	
	public static String TrendWindownExtract(int idSlot){
		String list = null;
		int slot = idSlot -1;
		int countSlot = (slideWindows-halfSlideWindows) -1;
		while (countSlot > 0){    
			if (list == null) list = " "+ slot;
			else  list += ", "+ slot;

			slot -= 1;
			countSlot -= 1;	            
		}
		return list;
	}
	
	public static String HistoryWindownExtract(int idSlot){
		String list = null;
		int slot = idSlot;
		for (int count = 0; count <(slideWindows-halfSlideWindows); count++ ){
			slot -= 1;
		}
		list = " " + slot;
		slot -= 1;
		int countSlot = halfSlideWindows -1;			
		while (countSlot > 0){    
			if (list == null) list = " "+ slot;
			else  list += ", "+ slot;

			slot -= 1;
			countSlot -= 1;	            
		}
		return list;
	}
	




	@SuppressWarnings("serial")
	public static void main(String[] args) {

		log.info("start");
		SparkConf sparkConf = new SparkConf().setAppName("Spark_Documents_Extract_slotHour").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		Date dataSlot = new Date();
		Long current =System.currentTimeMillis();
		try { //only to test
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataSlot = sdf.parse("2016-04-06 09:45:04");
			Instant instant = Instant.ofEpochMilli(dataSlot.getTime());
			current = instant.getEpochSecond() * 1000l;
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		final Long  nf = 60L * 60;
		final Long  nd = 60L * (60*24);
		final int slotHourId = (int) (((double)(current / 1000L)) / nf);
		final int slotDayId = (int) (((double)(current / 1000L)) / nd);

		try {
			Configuration mongodbConfigInput_IdCell = new Configuration();
			mongodbConfigInput_IdCell.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
			mongodbConfigInput_IdCell.set("mongo.input.uri", mongoConnInputTopicsCellId);
			mongodbConfigInput_IdCell.set("mongo.input.query","{SlotHourId :"+slotHourId+" }");

			Configuration mongodbConfigInput_Lac = new Configuration();
			mongodbConfigInput_Lac.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
			mongodbConfigInput_Lac.set("mongo.input.uri", mongoConnInputTopicstLac);
			mongodbConfigInput_Lac.set("mongo.input.query","{SlotHourId :"+slotHourId+" }");  //in  where  requets.SlotTimeId in ( {0} )



			JavaPairRDD<Object, BSONObject> documents_idCell = sc.newAPIHadoopRDD(mongodbConfigInput_IdCell,MongoInputFormat.class,Object.class,BSONObject.class);
			JavaRDD<DocumentsSchema> documentsEntry_IdCell = documents_idCell.map(
					new Function<Tuple2<Object,BSONObject>,DocumentsSchema>() {
						public DocumentsSchema call(Tuple2<Object,BSONObject> doc) throws Exception {

							DocumentsSchema documetSchema = new DocumentsSchema(null,doc._2.get("cellId").toString(), 
									Integer.valueOf(doc._2.get("SlotTimeId").toString()), 
									Integer.valueOf(doc._2.get("SlotHourId").toString()),
									null, null, 
									(ArrayList<String>) doc._2.get("terns"), 
									null);

							return documetSchema;
						}
					}

					);

			SQLContext sqlContext = new HiveContext(sc);


			DataFrame dfbase = sqlContext.createDataFrame(documentsEntry_IdCell, DocumentsSchema.class);
			dfbase.registerTempTable("TrendTable");
			
			String hql_2 = "Select  TrendTable.LAC,  TrendTable.categories, "+
	                       " AVG(TrendTable.Count) as mean, stddev_pop(TrendTable.Count) as variance from TrendTable "+
	                       " where  TrendTable.SlotTimeId in ( { " +  HistoryWindownExtract(slotHourId) + "} )  group By TrendTable.LAC, TrendTable.categories Having variance > 0  ";
			DataFrame baseMeanVarianceLocal = sqlContext.sql(hql_2);
		    baseMeanVarianceLocal.registerTempTable("MeanVarianceLocal");
		    
		    
		    
		    String hql_2a = "Select  TrendTable.LAC,   "+
	                 " AVG(TrendTable.Count) as mean, stddev_pop(TrendTable.Count) as variance from TrendTable "+
	                " where  TrendTable.SlotTimeId in ( { " +  HistoryWindownExtract(slotHourId) + " } )  group By TrendTable.LAC  Having variance > 0";
		    
		    DataFrame baseMeanVarianceGlobal = sqlContext.sql(hql_2a);
		    baseMeanVarianceGlobal.registerTempTable("MeanVarianceGlobal");
		    
		    
		   sqlContext.udf().register("Zscore", new UDF3<Double, Double, Double,Double>() {
	
				@Override
				public Double call(Double t1, Double t2, Double t3) throws Exception {
					  if  (t3 > 0)
				            return (t1-t2) / t3;
				        else
				            return Double.valueOf(0);
				}
			    }, DataTypes.DoubleType);

		    String hql_3 = "Select  TrendTable.LAC,  TrendTable.categories, "+
	                	" AVG(Zscore(TrendTable.Count, MeanVarianceLocal.mean, MeanVarianceLocal.variance) ) as ZscoreLocal, "+
	                	" AVG(Zscore(TrendTable.Count, MeanVarianceGlobal.mean, MeanVarianceGlobal.variance) ) as ZscoreGlobal "+
	                	" from TrendTable " +
	                	" JOIN MeanVarianceLocal  ON " +
	                    " TrendTable.LAC = MeanVarianceLocal.LAC and  " +
	                    " TrendTable.categories = MeanVarianceLocal.categories  " +
	                    " JOIN MeanVarianceGlobal  ON " +
	                    " TrendTable.LAC = MeanVarianceGlobal.LAC " +
	                    " where  TrendTable.SlotTimeId in ( { " +  TrendWindownExtract(slotHourId) + "} )  group By TrendTable.LAC, TrendTable.categories";
		    
		    DataFrame baseTrendTopicsResult = sqlContext.sql(hql_3);
		    baseTrendTopicsResult.registerTempTable("TrendTopic");
		    
		    
		    String hql_e = " SELECT LAC, categories, ZscoreLocal, rank " +
		            		" FROM ( " +
		            		" SELECT *, ROW_NUMBER() OVER ( " +
		                    " PARTITION BY LAC  ORDER BY ZscoreLocal DESC ) rank FROM TrendTopic " +
		                    " ) TrendTopic WHERE rank  <=  "+ topicsToRank + " ORDER BY LAC, rank ";
		    DataFrame  baseTrendTopicsRanked = sqlContext.sql(hql_e);
		    log.info("fim");
		    baseTrendTopicsRanked.show(1000);
		} catch (Exception e) {
			log.fatal(e);
			System.out.println(e);			
			
		}

	}

}

