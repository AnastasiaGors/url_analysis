package it.unitn.url.analysis.document.common;

import java.util.ArrayList;

import scala.Serializable;

public class RequestSchema implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 714833203138618952L;
	private String  recordOpeningDate = null;
	private Integer rATType = null; 
	private Integer  SlotTimeId = null;
	private String mcc = null;
	private String mnc = null;
	private String lac = null;
	private String cid = null;
	private String cellId = null;
	private String BrowsingSession = null;
	ArrayList<String> urls = null;
	String terns = null;
	public String getRecordOpeningDate() {
		return recordOpeningDate;
	}
	public void setRecordOpeningDate(String recordOpeningDate) {
		this.recordOpeningDate = recordOpeningDate;
	}
	public Integer getrATType() {
		return rATType;
	}
	public void setrATType(Integer rATType) {
		this.rATType = rATType;
	}
	public Integer getSlotTimeId() {
		return SlotTimeId;
	}
	public void setSlotTimeId(Integer slotTimeId) {
		SlotTimeId = slotTimeId;
	}

	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getMnc() {
		return mnc;
	}
	public void setMnc(String mnc) {
		this.mnc = mnc;
	}
	public String getLac() {
		return lac;
	}
	public void setLac(String lac) {
		this.lac = lac;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public ArrayList<String> getUrls() {
		return urls;
	}
	public void setUrls(ArrayList<String> urls) {
		this.urls = urls;
	}
	public String getTerns() {
		return terns;
	}
	public void setTerns(String terns) {
		this.terns = terns;
	}
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	
	public String getBrowsingSession() {
		return BrowsingSession;
	}
	
	public void setBrowsingSession(String browsingSession) {
		BrowsingSession = browsingSession;
	}
	public RequestSchema(String recordOpeningDate, Integer rATType, Integer slotTimeId, String mcc, String mnc, String lac,
			String cid, String cellId, String browsingSession, ArrayList<String> urls, String terns) {
		super();
		this.recordOpeningDate = recordOpeningDate;
		this.rATType = rATType;
		SlotTimeId = slotTimeId;
		this.mcc = mcc;
		this.mnc = mnc;
		this.lac = lac;
		this.cid = cid;
		this.cellId = cellId;
		BrowsingSession = browsingSession;
		this.urls = urls;
		this.terns = terns;
	}
	@Override
	public String toString() {
		return "Request [recordOpeningDate=" + recordOpeningDate + ", rATType=" + rATType + ", SlotTimeId=" + SlotTimeId
				+ ", mcc=" + mcc + ", mnc=" + mnc + ", lac=" + lac + ", cid=" + cid + ", cellId=" + cellId
				+ ", BrowsingSession=" + BrowsingSession + ", urls=" + urls + ", terns=" + terns + "]";
	}


	

}
