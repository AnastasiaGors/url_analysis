package it.unitn.url.analysis.document.common;

import java.util.ArrayList;

public class DocumentsSchema {
	private String lac = null;	
	private String cellId = null;
	private Integer slotTimeId = null;
	private Integer slotHourId = null;
	private Integer slotDayId = null;
	private Integer slotMonthId = null;
	public String getLac() {
		return lac;
	}
	public void setLac(String lac) {
		this.lac = lac;
	}
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	public Integer getSlotTimeId() {
		return slotTimeId;
	}
	public void setSlotTimeId(Integer slotTimeId) {
		this.slotTimeId = slotTimeId;
	}
	public Integer getSlotHourId() {
		return slotHourId;
	}
	public void setSlotHourId(Integer slotHourId) {
		this.slotHourId = slotHourId;
	}
	private ArrayList<String> terms = null;
	private ArrayList<String> urls = null;
	
	public ArrayList<String> getTerms() {
		return terms;
	}
	public void setTerms(ArrayList<String> terms) {
		this.terms = terms;
	}
	public ArrayList<String> getUrls() {
		return urls;
	}
	public void setUrls(ArrayList<String> urls) {
		this.urls = urls;
	}
	public Integer getSlotDayId() {
		return slotDayId;
	}
	public void setSlotDayId(Integer slotDayId) {
		this.slotDayId = slotDayId;
	}
	public Integer getSlotMonthId() {
		return slotMonthId;
	}
	public void setSlotMonthId(Integer slotMonthId) {
		this.slotMonthId = slotMonthId;
	}
	public DocumentsSchema(String lac, String cellId, Integer slotTimeId, Integer slotHourId, Integer slotDayId,
			Integer slotMonthId, ArrayList<String> terms, ArrayList<String> urls) {
		super();
		this.lac = lac;
		this.cellId = cellId;
		this.slotTimeId = slotTimeId;
		this.slotHourId = slotHourId;
		this.slotDayId = slotDayId;
		this.slotMonthId = slotMonthId;
		this.terms = terms;
		this.urls = urls;
	}

	
}
