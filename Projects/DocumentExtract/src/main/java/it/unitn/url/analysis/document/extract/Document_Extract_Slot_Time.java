package it.unitn.url.analysis.document.extract;
import static org.apache.spark.sql.functions.collect_list;
import static org.apache.spark.sql.functions.expr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;

import com.mongodb.hadoop.MongoInputFormat;
import com.mongodb.hadoop.MongoOutputFormat;

import it.unitn.url.analysis.document.common.AppendList;
import it.unitn.url.analysis.document.common.ConcatString;
import it.unitn.url.analysis.document.common.FieldsDto;
import it.unitn.url.analysis.document.common.RequestSchema;
import scala.Tuple2;
import scala.collection.mutable.WrappedArray;
public final class Document_Extract_Slot_Time {
	final static int  slotTime = 5;
	final static int  HOUR = 60;
	static int  DAY = 3600;
	final static Long  slotTimeInSeconds = 60L * slotTime;
	final static Long  slotHourInSeconds = 60L * HOUR;
	final static String mongoConnInput = "mongodb://localhost:27017/local.newEntry";
	final static String mongoConnOutputCellId = "mongodb://localhost:27017/local.documents_slotTime_idCell";
	final static String mongoConnOutputTopicsCellId = "mongodb://localhost:27017/local.topics_slotTime_idCell";
	final static String mongoConnOutputLac= "mongodb://localhost:27017/local.documents_slotTime_lac";
	final static String mongoConnOutpuTopicstLac= "mongodb://localhost:27017/local.topics_slotTime_lac";



	@SuppressWarnings("serial")
	public static void main(String[] args) {

		SparkConf sparkConf = new SparkConf().setAppName("Spark_Documents_Extract_slotTime").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		Date dataSlot = new Date();
		Long current =System.currentTimeMillis();
		try { //only to test
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataSlot = sdf.parse("2016-04-06 09:45:04");
			Instant instant = Instant.ofEpochMilli(dataSlot.getTime());
			current = instant.getEpochSecond() * 1000l;
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		final int slotTimeId = (int) (((double)(current / 1000L)) / slotTimeInSeconds); 
		final int slotHourId = (int) (((double)(current / 1000L)) / slotHourInSeconds);
		try {

			Configuration mongodbConfigInput = new Configuration();
			mongodbConfigInput.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
			mongodbConfigInput.set("mongo.input.uri", mongoConnInput);

			mongodbConfigInput.set("mongo.input.query","{SlotTimeId :"+slotTimeId+" }");

			Configuration mongodbConfigoOutputCelId = new Configuration();
			mongodbConfigoOutputCelId.set("mongo.job.output.format", "com.mongodb.hadoop.MongoOutputFormat");
			mongodbConfigoOutputCelId.set("mongo.output.uri", mongoConnOutputCellId);
			
			Configuration mongodbConfigoOutputTopicsCelId = new Configuration();
			mongodbConfigoOutputTopicsCelId.set("mongo.job.output.format", "com.mongodb.hadoop.MongoOutputFormat");
			mongodbConfigoOutputTopicsCelId.set("mongo.output.uri", mongoConnOutputTopicsCellId);		

			Configuration mongodbConfigoOutputLac = new Configuration();
			mongodbConfigoOutputLac.set("mongo.job.output.format", "com.mongodb.hadoop.MongoOutputFormat");
			mongodbConfigoOutputLac.set("mongo.output.uri", mongoConnOutputLac);	
			
			Configuration mongodbConfigoOutpuTopicsLac = new Configuration();
			mongodbConfigoOutpuTopicsLac.set("mongo.job.output.format", "com.mongodb.hadoop.MongoOutputFormat");
			mongodbConfigoOutpuTopicsLac.set("mongo.output.uri", mongoConnOutpuTopicstLac);
			
			

			JavaPairRDD<Object, BSONObject> documents = sc.newAPIHadoopRDD(mongodbConfigInput,MongoInputFormat.class,Object.class,BSONObject.class);

			JavaRDD<RequestSchema> documentsEntry = documents.map(
					new Function<Tuple2<Object,BSONObject>,RequestSchema>() {
						public RequestSchema call(Tuple2<Object,BSONObject> doc) throws Exception {

							String terms = "";
							ArrayList<String> urls = new ArrayList<String>();
							ArrayList<BSONObject> urlsList = (ArrayList<BSONObject>) doc._2.get("Urls");
							for (BSONObject obj :  urlsList){											
								urls.add(obj.get("name").toString());
								ArrayList<String> o  = (ArrayList<String>) obj.get("categories");								
								for (String s : o) terms += s + " ";			
							}							
							BSONObject userloc =(BSONObject) doc._2.get("UserLocation");
							String mcc = userloc.get("MCC").toString();
							String mnc = userloc.get("MNC").toString();
							String lac = userloc.get("LAC").toString();
							String cid = userloc.get("CID").toString();
							String idCell = mcc+mnc+lac+cid;
							RequestSchema rj = new RequestSchema(doc._2.get("RecordOpeningDate").toString(),
									Integer.valueOf(doc._2.get("rATType").toString()),
									Integer.valueOf(doc._2.get("SlotTimeId").toString()),
									mcc,mnc,lac,cid,idCell,doc._2.get("BrowsingSession").toString(),urls,terms);
							return  rj;		
						}
					}

					);

			SQLContext sqlContext = new HiveContext(sc);
			sqlContext.udf().register("concatString",new ConcatString()); 
			sqlContext.udf().register("appenList",new AppendList());

			DataFrame dfbase = sqlContext.createDataFrame(documentsEntry, RequestSchema.class);
			DataFrame dfbaseGroupByUser= dfbase.select("*").groupBy("browsingSession","cellId","lac").agg(expr("concatString(terns) as terns"),expr("appenList(urls) as urls"));			

			DataFrame dfbaseCellId = dfbaseGroupByUser.select("cellId","terns","lac","urls").groupBy("cellId","lac").agg(collect_list("terns").alias("terns"), expr("appenList(urls) as urls"));


			DataFrame  dfbaseLac =    dfbaseCellId.select("cellId","lac","terns","urls").groupBy("lac").agg(expr("appenList(terns) as terns"),expr("appenList(urls) as urls"));

			JavaRDD<Row> dfbaseNewRddCellId = dfbaseCellId.toJavaRDD();
			JavaRDD<Row> dfbaseNewRddLac    = dfbaseLac.toJavaRDD();

			//extract the topics by CellId

			JavaRDD<FieldsDto> tupleData = dfbaseNewRddCellId.map(line -> {				
				WrappedArray wa_1 = (WrappedArray) line.get(2);
				ArrayList<String> al_1 = new ArrayList<String>();
				for (int i = 0; i < wa_1.length(); i++) {
					al_1.add((String) wa_1.apply(i));
				}
				return new FieldsDto(line.get(0).toString(), null, al_1);				
			});

			ArrayList<ArrayList<String>> resultTopcis_CellId =  new ArrayList<>();
			
			tupleData.collect().forEach( field -> resultTopcis_CellId.add(Extract_Topics.extractor(sc, field)));

			JavaRDD<ArrayList<String>> ptopics = sc.parallelize(resultTopcis_CellId);
			
			JavaPairRDD<Object,BSONObject> insertTopicOnMongoCellId = ptopics.mapToPair(
					new PairFunction<ArrayList<String>, Object, BSONObject>() {
						@Override
						public Tuple2<Object, BSONObject> call(ArrayList<String> list) throws Exception {	
						
							String id = list.get(0);
							list.remove(0);

							BSONObject doc = new BasicBSONObject();
							doc.put("cellId",id);
							doc.put("SlotTimeId", slotTimeId);
							doc.put("SlotHourId", slotHourId);
							doc.put("topics",list);							
							return new Tuple2<Object, BSONObject>(null, doc);
						}
					}
					); 
			insertTopicOnMongoCellId.saveAsNewAPIHadoopFile("file:///notapplicable",Object.class, Object.class, MongoOutputFormat.class, mongodbConfigoOutputTopicsCelId);


			//extract the topics by Lac

			
			JavaRDD<FieldsDto> tupleDataLac = dfbaseNewRddLac.map(line -> {				
				WrappedArray wa_1 = (WrappedArray) line.get(2);
				ArrayList<String> al_1 = new ArrayList<String>();
				for (int i = 0; i < wa_1.length(); i++) {
					al_1.add((String) wa_1.apply(i));
				}
				return new FieldsDto(line.get(0).toString(), null, al_1);				
			});

			ArrayList<ArrayList<String>> resultTopcis_LAC =  new ArrayList<>();
			
			tupleDataLac.collect().forEach( field -> {resultTopcis_LAC.add(Extract_Topics.extractor(sc, field));  System.out.println(resultTopcis_LAC.size());});

			JavaRDD<ArrayList<String>> ptopics_lac = sc.parallelize(resultTopcis_LAC);
			
			JavaPairRDD<Object,BSONObject> insertTopicOnMongoLac = ptopics_lac.mapToPair(
					new PairFunction<ArrayList<String>, Object, BSONObject>() {
						@Override
						public Tuple2<Object, BSONObject> call(ArrayList<String> list) throws Exception {	
						
							String id = list.get(0);
							list.remove(0);

							BSONObject doc = new BasicBSONObject();
							doc.put("lac",id);
							doc.put("SlotTimeId", slotTimeId);
							doc.put("SlotHourId", slotHourId);
							doc.put("topics",list);							
							return new Tuple2<Object, BSONObject>(null, doc);
						}
					}
					); 
			insertTopicOnMongoLac.saveAsNewAPIHadoopFile("file:///notapplicable",Object.class, Object.class, MongoOutputFormat.class, mongodbConfigoOutpuTopicsLac);



			//save term by cellid			
			JavaPairRDD<Object,BSONObject> insertOnMongoCellId = dfbaseNewRddCellId.mapToPair(
					new PairFunction<Row, Object, BSONObject>() {
						@Override
						public Tuple2<Object, BSONObject> call(Row line) throws Exception {	
							WrappedArray wa_1 =(WrappedArray) line.get(2);
							ArrayList<String> al_1 = new ArrayList<String>();			                	
							for (int i= 0; i< wa_1.length(); i++){
								al_1.add((String) wa_1.apply(i));
							}

							WrappedArray wa_2 =(WrappedArray) line.get(3);
							ArrayList<String> al_2 = new ArrayList<String>();			                	
							for (int i= 0; i< wa_2.length(); i++){
								al_2.add((String) wa_2.apply(i));
							}


							BSONObject doc = new BasicBSONObject();
							doc.put("cellId",line.get(0).toString());
							doc.put("SlotTimeId", slotTimeId);
							doc.put("SlotHourId", slotHourId);
							doc.put("terns",al_1);
							doc.put("urls",al_2);
							return new Tuple2<Object, BSONObject>(null, doc);
						}
					}
					);        	
			insertOnMongoCellId.saveAsNewAPIHadoopFile("file:///notapplicable",Object.class, Object.class, MongoOutputFormat.class, mongodbConfigoOutputCelId);		        


			//save term by lac
			JavaPairRDD<Object,BSONObject> insertOnMongoLac = dfbaseNewRddLac.mapToPair(
					new PairFunction<Row, Object, BSONObject>() {
						@Override
						public Tuple2<Object, BSONObject> call(Row line) throws Exception {		
							BSONObject doc = new BasicBSONObject();
							WrappedArray wa_1 =(WrappedArray) line.get(1);
							ArrayList<String> al_1 = new ArrayList<String>();			                	
							for (int i= 0; i< wa_1.length(); i++){
								al_1.add((String) wa_1.apply(i));
							}
							WrappedArray wa_2 =(WrappedArray) line.get(2);
							ArrayList<String> al_2 = new ArrayList<String>();			                	
							for (int i= 0; i< wa_2.length(); i++){
								al_2.add((String) wa_2.apply(i));
							}


							doc.put("lac",line.get(0).toString());
							doc.put("SlotTimeId", slotTimeId);
							doc.put("SlotHourId", slotHourId);
							doc.put("terns",al_1);
							doc.put("urls",al_2);

							return new Tuple2<Object, BSONObject>(null, doc);
						}
					}
					);   
			insertOnMongoLac.saveAsNewAPIHadoopFile("file:///notapplicable",Object.class, Object.class, MongoOutputFormat.class, mongodbConfigoOutputLac);

		} catch (Exception e) {
			System.out.println(e);
		}

		sc.stop();
		sc.close();
		System.out.println("Spark_Documents_Extract_slotTime - End");
		System.exit(0);
	}
}

