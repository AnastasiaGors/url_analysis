package it.unitn.url.analysis.document.extract;


import static org.apache.spark.sql.functions.expr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;

import com.mongodb.hadoop.MongoInputFormat;
import com.mongodb.hadoop.MongoOutputFormat;

import it.unitn.url.analysis.document.common.AppendList;
import it.unitn.url.analysis.document.common.ConcatString;
import it.unitn.url.analysis.document.common.DocumentsSchema;
import scala.Tuple2;
import scala.collection.mutable.WrappedArray;
public final class Document_Extract_Slot_Hour {
	private static String mongoConnInputCellId = "mongodb://localhost:27017/local.documents_slotTime_idCell";
	private static String mongoConnInputLac=     "mongodb://localhost:27017/local.documents_slotTime_lac";
	
	private static String mongoConnOutputCellId = "mongodb://localhost:27017/local.documents_slotHour_idCell";
	private static String mongoConnOutputLac=     "mongodb://localhost:27017/local.documents_slotHour_lac";
	
	

	@SuppressWarnings("serial")
	public static void main(String[] args) {

		SparkConf sparkConf = new SparkConf().setAppName("Spark_Documents_Extract_slotHour").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		Date dataSlot = new Date();
		Long current =System.currentTimeMillis();
		try { //only to test
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataSlot = sdf.parse("2016-04-06 09:45:04");
			Instant instant = Instant.ofEpochMilli(dataSlot.getTime());
			current = instant.getEpochSecond() * 1000l;
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		final Long  nf = 60L * 60;
		final Long  nd = 60L * (60*24);
		final int slotHourId = (int) (((double)(current / 1000L)) / nf);
		final int slotDayId = (int) (((double)(current / 1000L)) / nd);
		
		try {

			Configuration mongodbConfigInput_IdCell = new Configuration();
			mongodbConfigInput_IdCell.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
			mongodbConfigInput_IdCell.set("mongo.input.uri", mongoConnInputCellId);
			mongodbConfigInput_IdCell.set("mongo.input.query","{SlotHourId :"+slotHourId+" }");
			
			Configuration mongodbConfigInput_lac = new Configuration();
			mongodbConfigInput_lac.set("mongo.job.input.format", "com.mongodb.hadoop.MongoInputFormat");
			mongodbConfigInput_lac.set("mongo.input.uri", mongoConnInputLac);					
			mongodbConfigInput_lac.set("mongo.input.query","{SlotHourId :"+slotHourId+" }");
			
			Configuration mongodbConfigoOutputCelId = new Configuration();
			mongodbConfigoOutputCelId.set("mongo.job.output.format", "com.mongodb.hadoop.MongoOutputFormat");
			mongodbConfigoOutputCelId.set("mongo.output.uri", mongoConnOutputCellId);		
			
			Configuration mongodbConfigoOutputLac = new Configuration();
			mongodbConfigoOutputLac.set("mongo.job.output.format", "com.mongodb.hadoop.MongoOutputFormat");
			mongodbConfigoOutputLac.set("mongo.output.uri", mongoConnOutputLac);
			


		
			JavaPairRDD<Object, BSONObject> documents_idCell = sc.newAPIHadoopRDD(mongodbConfigInput_IdCell,MongoInputFormat.class,Object.class,BSONObject.class);
			JavaRDD<DocumentsSchema> documentsEntry_IdCell = documents_idCell.map(
					new Function<Tuple2<Object,BSONObject>,DocumentsSchema>() {
						public DocumentsSchema call(Tuple2<Object,BSONObject> doc) throws Exception {

							DocumentsSchema documetSchema = new DocumentsSchema(null,doc._2.get("cellId").toString(), 
																						Integer.valueOf(doc._2.get("SlotTimeId").toString()), 
																						Integer.valueOf(doc._2.get("SlotHourId").toString()),
																						null, null, 
																						(ArrayList<String>) doc._2.get("terns"), 
																						(ArrayList<String>) doc._2.get("urls"));
							
									return documetSchema;
						}
					}

					);
			  
			SQLContext sqlContext = new HiveContext(sc);
			sqlContext.udf().register("concatString",new ConcatString()); 
			sqlContext.udf().register("appenList",new AppendList());
			
			DataFrame dfbase = sqlContext.createDataFrame(documentsEntry_IdCell, DocumentsSchema.class);					
			DataFrame dfbaseCellId = dfbase.select("cellId","terms", "urls").groupBy("cellId").agg(expr("appenList(terms) as terms"), expr("appenList(urls) as urls"));
						
			JavaRDD<Row> dfbaseNewRddCellId =  dfbaseCellId.toJavaRDD();					
			JavaPairRDD<Object,BSONObject> insertOnMongoCellId = dfbaseNewRddCellId.mapToPair(
			            new PairFunction<Row, Object, BSONObject>() {
			                @Override
			                public Tuple2<Object, BSONObject> call(Row line) throws Exception {	
			                  	WrappedArray wa_1 =(WrappedArray) line.get(1);
			                	ArrayList<String> al_1 = new ArrayList<String>();			                	
			                	for (int i= 0; i< wa_1.length(); i++){
			                		al_1.add((String) wa_1.apply(i));
			                	}
			                	WrappedArray wa_2 =(WrappedArray) line.get(2);
			                	ArrayList<String> al_2 = new ArrayList<String>();			                	
			                	for (int i= 0; i< wa_2.length(); i++){
			                		al_2.add((String) wa_2.apply(i));
			                	}
		                	
			                	
			                	BSONObject doc = new BasicBSONObject();
			                    doc.put("cellId",line.get(0).toString());			                    
			                    doc.put("SlotHourId", slotHourId);
			                    doc.put("SlotDayId", slotDayId);			                    
			                    doc.put("terns",al_1);
			                    doc.put("urls",al_2);
			                    return new Tuple2<Object, BSONObject>(null, doc);
			                }
			            }
			        );        	
		      insertOnMongoCellId.saveAsNewAPIHadoopFile("file:///notapplicable",Object.class, Object.class, MongoOutputFormat.class, mongodbConfigoOutputCelId);

		        
		        
		      
		      
		      
		      
		      
		      JavaPairRDD<Object, BSONObject> documents_Lac = sc.newAPIHadoopRDD(mongodbConfigInput_lac,MongoInputFormat.class,Object.class,BSONObject.class);
				JavaRDD<DocumentsSchema> documentsEntry_lac = documents_Lac.map(
						new Function<Tuple2<Object,BSONObject>,DocumentsSchema>() {
							public DocumentsSchema call(Tuple2<Object,BSONObject> doc) throws Exception {
								
								DocumentsSchema documetSchema = new DocumentsSchema(doc._2.get("lac").toString(),null, 
																							Integer.valueOf(doc._2.get("SlotTimeId").toString()), 
																							Integer.valueOf(doc._2.get("SlotHourId").toString()),
																							null, null, 
																							(ArrayList<String>) doc._2.get("terns"), 
																							(ArrayList<String>) doc._2.get("urls"));
								
										return documetSchema;
							}
						}

						);
				  
				
				DataFrame dfbase_lac = sqlContext.createDataFrame(documentsEntry_lac, DocumentsSchema.class);					
				DataFrame dfbaselac = dfbase_lac.select("lac","terms", "urls").groupBy("lac").agg(expr("appenList(terms) as terms"), expr("appenList(urls) as urls"));
							
				JavaRDD<Row> dfbaseNewRddlac =  dfbaselac.toJavaRDD();					
				JavaPairRDD<Object,BSONObject> insertOnMongolac = dfbaseNewRddlac.mapToPair(
				            new PairFunction<Row, Object, BSONObject>() {
				                @Override
				                public Tuple2<Object, BSONObject> call(Row line) throws Exception {	
				                  	WrappedArray wa_1 =(WrappedArray) line.get(1);
				                	ArrayList<String> al_1 = new ArrayList<String>();			                	
				                	for (int i= 0; i< wa_1.length(); i++){
				                		al_1.add((String) wa_1.apply(i));
				                	}
				                	WrappedArray wa_2 =(WrappedArray) line.get(2);
				                	ArrayList<String> al_2 = new ArrayList<String>();			                	
				                	for (int i= 0; i< wa_2.length(); i++){
				                		al_2.add((String) wa_2.apply(i));
				                	}
			                	
				                	
				                	BSONObject doc = new BasicBSONObject();
				                    doc.put("lac",line.get(0).toString());			                    
				                    doc.put("SlotHourId", slotHourId);
				                    doc.put("SlotDayId", slotDayId);			                    
				                    doc.put("terns",al_1);
				                    doc.put("urls",al_2);
				                    return new Tuple2<Object, BSONObject>(null, doc);
				                }
				            }
				        );        	
				insertOnMongolac.saveAsNewAPIHadoopFile("file:///notapplicable",Object.class, Object.class, MongoOutputFormat.class, mongodbConfigoOutputLac);

			        
		     

		} catch (Exception e) {
			System.out.println(e);
		}

		sc.stop();
		sc.close();
		System.out.println("Spark_Documents_Extract_slotTime - End");
		System.exit(0);
	}
}
