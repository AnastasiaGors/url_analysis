package it.unitn.url.analysis.document.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.MutableAggregationBuffer;
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class AppendList extends UserDefinedAggregateFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 468103553360791290L;

	private StructType _inputDataType;
	private StructType _bufferSchema;
	private DataType _returnDataType;

	public AppendList() {
		List<StructField> inputFields = new ArrayList<StructField>();
		inputFields
				.add(DataTypes.createStructField("inputList", DataTypes.createArrayType(DataTypes.StringType), true));
		_inputDataType = DataTypes.createStructType(inputFields);

		List<StructField> bufferFields = new ArrayList<StructField>();
		bufferFields
				.add(DataTypes.createStructField("bufferList", DataTypes.createArrayType(DataTypes.StringType), true));
		_bufferSchema = DataTypes.createStructType(bufferFields);

		_returnDataType = DataTypes.createArrayType(DataTypes.StringType);
	}

	@Override
	public StructType bufferSchema() {
		// TODO Auto-generated method stub
		return _bufferSchema;
	}

	@Override
	public DataType dataType() {
		// TODO Auto-generated method stub
		return _returnDataType;
	}

	@Override
	public boolean deterministic() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Object evaluate(Row arg0) {
		if (arg0.isNullAt(0)) {
			return null;
		} else {		
			List<String> list_0  =arg0.getList(0);
			ArrayList<String> oo = new ArrayList<>();
			oo.addAll(list_0);
			return oo;
			
		}
	}

	@Override
	public void initialize(MutableAggregationBuffer arg0) {
		arg0.update(0, new ArrayList<String>());

	}

	@Override
	public StructType inputSchema() {
		// TODO Auto-generated method stub
		return _inputDataType;
	}

	@Override
	public void merge(MutableAggregationBuffer arg0, Row arg1) {

		if (!arg0.isNullAt(0)) {
			if (arg0.isNullAt(0)) {	
				List<String> list_1  =arg1.getList(0);
				ArrayList<String> oo = new ArrayList<>();
				oo.addAll(list_1);
				arg0.update(0,oo);
			} else {
				List<String> list_0  =  arg0.getList(0);
				List<String> list_1 = arg1.getList(0);
				ArrayList<String> oo = new ArrayList<>();
				oo.addAll(list_0);
				oo.addAll(list_1);
				arg0.update(0,oo);				
			}
		}
	}

	@Override
	public void update(MutableAggregationBuffer arg0, Row arg1) {

		if (!arg1.isNullAt(0)) {
			if (arg0.isNullAt(0)) {		
				List<String> list_1  =arg1.getList(0);
				ArrayList<String> oo = new ArrayList<>();
				oo.addAll(list_1);
				arg0.update(0,oo);
			} else {				
				List<String> list_0  =arg0.getList(0);
				List<String> list_1 = arg1.getList(0);
				ArrayList<String> oo = new ArrayList<>();
				oo.addAll(list_0);
				oo.addAll(list_1);				
				arg0.update(0, oo);
			}
		}

	}

}
