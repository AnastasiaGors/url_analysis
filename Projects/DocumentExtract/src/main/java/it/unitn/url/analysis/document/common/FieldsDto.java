package it.unitn.url.analysis.document.common;

import java.io.Serializable;
import java.util.ArrayList;

 public class FieldsDto implements Serializable, scala.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5695880012539140515L;
	String id = null;
	ArrayList<String> urls =null;
	
	ArrayList<String> terms = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<String> getUrls() {
		return urls;
	}

	public void setUrls(ArrayList<String> urls) {
		this.urls = urls;
	}

	public ArrayList<String> getTerms() {
		return terms;
	}

	public void setTerms(ArrayList<String> terms) {
		this.terms = terms;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Fields [id=" + id + ", urls=" + urls + ", terms=" + terms + "]";
	}

	public FieldsDto(String id, ArrayList<String> urls, ArrayList<String> terms) {
		super();
		this.id = id;
		this.urls = urls;
		this.terms = terms;
	}
	

	


}
