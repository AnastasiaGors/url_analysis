package it.unitn.url.analysis.document.extract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.clustering.DistributedLDAModel;
import org.apache.spark.mllib.clustering.LDA;
import org.apache.spark.mllib.feature.HashingTF;
import org.apache.spark.mllib.feature.IDF;
import org.apache.spark.mllib.feature.IDFModel;
import org.apache.spark.mllib.linalg.Matrix;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import it.unitn.url.analysis.document.common.FieldsDto;
import scala.Tuple2;

public class Extract_Topics implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5819468005717826267L;
	final static int qtaTOPICS = 1;
	final static double  minValueConsiderTOPICS = 10.0;
	final static double  minValueConsiderTF_IDF = 0.3;
	final static int  minDocFreq = 1;

	public static ArrayList<String> extractor(final JavaSparkContext sc, FieldsDto ele  ){

		ArrayList<String> topicsList = new ArrayList<String>(); 
		topicsList.add(ele.getId());
		
		ArrayList<String> val = ele.getTerms();		


		//start extract topics
		if (val.size() > 1) {
			try {


				//TF-IDF

				JavaRDD<String> o = sc.parallelize(val);

				JavaRDD<String> pp = o.flatMap(rox -> Arrays.asList(rox.split(" ")));
				JavaPairRDD<String, Integer> countWorld = pp.mapToPair(str -> new Tuple2<>(str, 1)).reduceByKey((a, b) -> a + b);
				countWorld.cache();									
				final Integer words = (int) countWorld.count();

				HashingTF hashingTF = new HashingTF(words);
				
								
				//da testare
				JavaRDD<List<String>> listaDocumentsTfIdf = o.map(rox -> Arrays.asList(rox.split(" ")));
				//JavaRDD<Vector> hashM = o.map(row -> hashingTF.transform(Arrays.asList(row.split(" "))));
				JavaRDD<Vector> hashM = hashingTF.transform(listaDocumentsTfIdf);
				List<Vector> eeer = hashM.collect();
				eeer.forEach(e -> System.out.println(e));
 							
				//test
				


				//extrairuma so vez de todo o documento
				JavaPairRDD<String, Integer> indexWord = countWorld.mapToPair(a -> new Tuple2<>(a._1, hashingTF.indexOf(a._1)));
				indexWord.cache();



				int[][] ldaMatrix = new int[val.size()][words];


				IDF idf = new IDF(minDocFreq);

				//slow
				IDFModel tfidf = idf.fit(hashM);
				JavaRDD<Vector> result = tfidf.transform(hashM);									
				

				JavaRDD<ArrayList<Integer>> resultFilter = result.map(row -> {
					ArrayList<Integer> r = new ArrayList<Integer>();
					int[] keys = row.toSparse().indices();
					double[] values = row.toSparse().values();
					int l = keys.length;
					for (int i = 0; i < l; i++) {
						if (values[i] > minValueConsiderTF_IDF) {
							r.add(keys[i]);
						}
					}
					return r;
				});

				java.util.List<ArrayList<Integer>> listResult = resultFilter.collect();
				for (int doc = 0; doc< listResult.size() ; doc++){
					ArrayList<Integer> row = listResult.get(doc);
					for (int index = 0; index< row.size() ; index++){
						int word =row.get(index);

						JavaPairRDD<String, Integer>  wordIndex = indexWord.filter(tupla -> {if  (tupla._2 == word){ return true;} else return false; });
						java.util.List<Tuple2<String,Integer>> key = wordIndex.collect();
						if ((key != null) && (key.size() > 0)){
							final String wordPar = key.get(0)._1;
							JavaPairRDD<String, Integer>  countValur = countWorld.filter(tupla -> {if  (tupla._1.equals(wordPar)){ return true;} else return false; });
							java.util.List<Tuple2<String,Integer>> keyCount = countValur.collect();
							if ((keyCount != null) && (keyCount.size() > 0)){
								ldaMatrix[doc][word] = keyCount.get(0)._2;
							}
						}
					}
				}

				ArrayList<Vector> ldalist = new ArrayList<Vector>();
				for (int doc = 0 ;doc <val.size() ; doc++){
					double[] values = new double[words];
					for (int x = 0 ;x <words; x++){											  
						values[x] =(Double.valueOf(ldaMatrix[doc][x]));
						ldalist.add(Vectors.dense(values));
					}
				}
				JavaRDD<Vector> ldaList = sc.parallelize(ldalist);

				JavaPairRDD<Long, Vector> corpus = JavaPairRDD.fromJavaRDD(ldaList.zipWithIndex().map(
						new Function<Tuple2<Vector, Long>, Tuple2<Long, Vector>>() {
							public Tuple2<Long, Vector> call(Tuple2<Vector, Long> doc_id) {
								return doc_id.swap();
							}
						}
						));
				corpus.cache();


				//LDA
				ArrayList<ArrayList<Integer>> topicResult = new ArrayList<ArrayList<Integer>>();
				ArrayList<String> topicStringResult = new ArrayList<String>();
				//very slow
				DistributedLDAModel ldaModel = (DistributedLDAModel) new LDA().setK(qtaTOPICS).run(corpus);


				Matrix topics = ldaModel.topicsMatrix();
				for (int topic = 0; topic < qtaTOPICS; topic++) {				    	 
					ArrayList<Integer> wordlsTopic = new  ArrayList<Integer>();
					String topicString = null;
					System.out.print("Topic " + topic + ":");
					for (int word = 0; word < ldaModel.vocabSize(); word++) {
						if (topics.apply(word, topic) > minValueConsiderTOPICS){
							final int x = word;
							JavaPairRDD<String, Integer>  wordIndex = indexWord.filter(tupla -> {if  (tupla._2 == x){ return true;} else return false; });
							java.util.List<Tuple2<String,Integer>> key = wordIndex.collect();
							if ((key != null) && (key.size() > 0)){									
								if (topicString == null){
									topicString = key.get(0)._1;
								}else{
									topicString = topicString + " " + key.get(0)._1;
								}
							}
							wordlsTopic.add(word);
						}
					}
					if (wordlsTopic.size() > 0){
						topicResult.add(wordlsTopic);
						topicStringResult.add(topicString);
					}				    	 
				}


				//extract topic : look the documents to find the topic				     
				for (int doc = 0; doc< val.size() ; doc++){							

					//topics
					for (int top = 0; top < topicResult.size(); top++){
						boolean found_Topic = true;

						//topics words
						int wordToc = 0;
						while ( (wordToc < topicResult.size())  &&  (!found_Topic)){
							int word = topicResult.get(top).get(wordToc);
							if (ldaMatrix[top][word] < 1)
								found_Topic =false;
							wordToc++;
						}
						if (found_Topic){								
							topicsList.add(topicStringResult.get(top));
						}

					}
				}
				

			} catch (Exception e) {
				System.out.println(e);
			}  

		}	//solo un documento cosa fare?
		topicsList.forEach(x -> System.out.println(x.toString()));
		return topicsList;
	}
}
