'''
Created on 09 Jun 2016

@author: anastasia
'''

from __future__ import print_function
from pyspark import SparkContext,SparkConf
from pyspark.sql import HiveContext
from pyspark.sql.functions import col,collect_list
from pyspark.mllib.clustering import KMeans, KMeansModel
from dateutil.parser import parse
from pyspark.mllib.feature import HashingTF, IDF
import time
from pyspark.mllib.linalg import Vectors
from numpy import array
from math import sqrt
import os


if __name__ == "__main__":

    os.environ["SPARK_HOME"] = "/Users/anastasia/Documents/spark-1.6.1-bin-hadoop2.6"
                                #/usr/hdp/current/spark-client
                                
    
# Append the python dir to PYTHONPATH so that pyspark could be found
    #sys.path.append('/usr/local/Cellar/apache-spark/spark-1.5.2-bin-hadoop2.6/python/')
# Append the python/build to PYTHONPATH so that py4j could be found
    #sys.path.append('/usr/local/Cellar/apache-spark/spark-1.5.2-bin-hadoop2.6/python/lib/py4j-0.8.2.1-src.zip')
    
    conf = SparkConf().setAppName("Spark Cluster").set("spark.executor.memory", "1g")
        #setMaster("spark://127.0.0.1:7077")
        #.setSparkHome('/Users/anastasia/Documents/spark-1.6.1-bin-hadoop2.6') \
        #.set('spark.executor.extraClassPath', '/Users/sestari/Documents/mongo-hadoop-core-1.5.2.jar') #;/Users/sestari/Documents/mongo-java-driver-3.0.4.jar;/Users/sestari/Documents/mongodb-driver-3.2.2.jar')

    sc = SparkContext(conf=conf) 
        
    sqlContext = HiveContext(sc)
    
    #slot time used by SlideWindow in minutes
    slotTime = 5
    #SlideWindows: used to determine how many slotTime to use
    slideWindows = 1
    
    dataNow = (parse('2016-04-06 09:45:04'))
  # datetime.strptime('2016-04-06 09:59:04', '%Y-%m-%d %H:%M:%S').date()

        
    def getUniversalSloTime(datep):
        #Unix time (seconds since January 1, 1970)
        timeUnix = time.mktime(datep.timetuple())
        slotTimeSecond = slotTime * 60
        return int(timeUnix // slotTimeSecond)
    #print(getUniversalSloTime(dataNow))
    
    def SlideWindonwExtract(idSlot):
        list = []
        slot = idSlot -1
        countSlot = slideWindows -1
        while (countSlot > 0):           
            list.append(slot)
            slot -= 1
            countSlot -= 1
        result = str(idSlot)
        for p in list:
            result += ", " + str(p)            
        return result
    #print(SlideWindonwExtract(getUniversalSloTime(dataNow)))

    def extractWord(x, indexes):
        output = []
        keys = x.indices
        values = x.values
        for i in xrange(0, len(keys)):
            if values[i] > 0.9:
                print()
                #world = filter(lambda (a,b): b == keys[i] ,indexes)
                output.append(indexes[keys[i]] )
        return output
    
    def kmeansMatrix(mapReduceList,kmeansList,keys):
        #d = [[("a",1),("b",2)],[("a",1),("b",2)]]
        #e =[[0,0],[0,0]]
        #f =["a","b"]
        indexDocument = 0
        for word in mapReduceList:
            for (a,b) in word:
                indexWord = 0
                found = False
                while found == False:
                    if keys[indexWord] == a :
                        found = True
                        kmeansList[indexDocument][indexWord] = b
                    else:                        
                        indexWord += 1
            indexDocument += 1
        return kmeansList   
     
    # Build k-means model (cluster the data)
    def kmeans(tfidf,keys,buckets,corpus_dict_tfidf_t):
        
        
#         print(tfdf.collect())
        group = tfidf.map(lambda line:  reduce(lambda a,b : a+b , [map(lambda y : (y,1), line)] ))
        mapReduceList = group.collect()
        kmeansList =  sc.parallelize(kmeansMatrix(mapReduceList,buckets,keys))        
        parsedData = kmeansList.map(lambda line: Vectors.dense(line))

#         corpus = parsedData.zipWithIndex().map(lambda x: [x[1], x[0]]).cache()
#         print(corpus.collect())

        kmeans_model = KMeans.train(parsedData, 10, maxIterations=10, runs=1, initializationMode="random")
        
# Make a prediction (which cluster it belongs)
        pred = parsedData.map(lambda point: kmeans_model.predict(point))
        clusterid = pred.collect()
        
#         iii = parsedData.collect()
#         for vector in iii:
#             print("cluster " + str(kmeans_model.predict(vector)) + ":" + str(vector))
        # Evaluate clustering by computing Within Set Sum of Squared Errors
        def error(point):
            center = kmeans_model.centers[kmeans_model.predict(point)]
            return sqrt(sum([x**2 for x in (point - center)]))
             
            WSSSE = parsedData.map(lambda point: error(point)).reduce(lambda x, y: x + y)
    #         dd = WSSSE.collect()
            print("Within Set Sum of Squared Error = " + str(WSSSE))
        
        return clusterid
    
        #Get words from numbers 
    def dataClusterPairs(data, cluster_id):
        result = []
        for i in xrange(len(data)):
            result.append([cluster_id[i], data[i]])
        return result
        
# Apply TF-IDF
    def newtfidf(documentsx,keys,corpus_dict_tfidf_t):
        
        hashingTF = HashingTF()
        tf = hashingTF.transform(documentsx)

        tf.cache()
        idf = IDF().fit(tf)
        tfidf = idf.transform(tf)
    
        idfIgnore = IDF(minDocFreq=2).fit(tf)
        tfidfIgnore = idf.transform(tf)

               
        words_all = keys
        for word in words_all:
            idx = hashingTF.indexOf(word)
            corpus_dict_tfidf_t[idx] =  word
      
        print(tfidf.collect())
        tfidf_mapping = tfidf.map(lambda x: extractWord(x, corpus_dict_tfidf_t))
        print(tfidf_mapping.collect())

        
        return tfidf_mapping
    
    def analiseDocument(documents):
#         print(documents)
        red =reduce(lambda x,y: x+y,documents)
        print(red)
        documentsLabeled =  map(lambda x: x.split(" "),red)
        print(documentsLabeled)
        documentsx = sc.parallelize(documentsLabeled)
        
        keysR = set(documentsx.reduce(lambda x, y: x + y)) 
        keys = list(set(keysR)) 
        
        #buckets = 0 * np.ones((len(red),len(keys)),dtype=np.int)  
        buckets = [[0 for col in range(len(keys))] for row in range(len(red))]
        corpus_dict_tfidf_t = {} 
        es = newtfidf(documentsx,keys,corpus_dict_tfidf_t)
        resultTF = es.flatMap(lambda x: x)
        print(resultTF.collect())
        #ee = resultTF.map(lambda x : (x,1)).reduceByKey(lambda x,y:x+y)
        #print(ee.collect())
        resultKmeans = kmeans(es,keys,buckets,corpus_dict_tfidf_t)       
        mydocuments = sc.parallelize(dataClusterPairs(documentsLabeled, resultKmeans))  
        clusters = mydocuments.collect()
        return "mydocuments"

    text = [["photo peter king boehner exit mean crazi taken arti"],["I wish Java could Spark use case classes"], ["Logistic regression Spark models are neat"],["d we"],["He wishes Java uses Spark case classes"],["photo image king exit mean crazi take"]]
    dr = analiseDocument(text)
    print(dr)

        
    def extractTerms(value) :        
        m =  map(lambda x :  x[0] ,value)
        r = reduce(lambda x, y: x + y, m)
        e =" ".join(r)
        e += " "
        return e
            
    sqlContext.registerFunction("extractTerms", extractTerms)
    #get json files
    dfRequest = sqlContext.read.json("result1464610228.83/part-00000")
    
    #main table
    dfRequest.registerTempTable("requests")

            
    #child table that contais the users Locatins information
    dfLocaBade =  dfRequest.select("GSN","ChargingID","RecordSequence", dfRequest.select("UserLocation")[0])  
    dfLocations = dfLocaBade.select("GSN","ChargingID","RecordSequence",\
                                    dfLocaBade.UserLocation.MCC.alias("MCC"),\
                                    dfLocaBade.UserLocation.MNC.alias("MNC"), \
                                    dfLocaBade.UserLocation.LAC.alias("LAC"),\
                                    dfLocaBade.UserLocation.CID.alias("CID") )                                    
    dfLocations.registerTempTable("UserLocation");
    dfLocations.show();

    
    #table contais the urls informations
    dfCategories =  sqlContext.sql("SELECT GSN, ChargingID, RecordSequence, extractTerms(Urls) as terms From requests ")
    dfCategories.registerTempTable("Urls");
    dfCategories.printSchema()
    #dfCategories.show()
    for row in dfCategories.collect():
        print(row)
    

    #sqlContext.registerFunction("analiseDoc", analiseDocument)
    

    
    hql =               "Select UserLocation.LAC, requests.SlotTimeId, requests.BrowsingSession , collect_list(Urls.terms) as documents from UserLocation \
                            JOIN Urls  ON \
                                Urls.GSN = UserLocation.GSN and  \
                                Urls.ChargingID = UserLocation.ChargingID and \
                                Urls.RecordSequence = UserLocation.RecordSequence \
                            JOIN requests  ON \
                                requests.GSN = Urls.GSN and  \
                                requests.ChargingID = Urls.ChargingID and \
                                requests.RecordSequence = Urls.RecordSequence "                                                     
    hql += "where  requests.SlotTimeId in ( {0} ) ".format(SlideWindonwExtract(getUniversalSloTime(dataNow)))
 
     
    hql += " group By  UserLocation.LAC, requests.SlotTimeId, requests.BrowsingSession  order by UserLocation.LAC"
    baseStory = sqlContext.sql(hql)
    baseStory.registerTempTable("TermTable")
    def listToString(value):         
        #red =map(lambda x,y: join(x),value)
        return " ".join(value)
    
    sqlContext.registerFunction("listToString", listToString)
    ee = sqlContext.sql(" select TermTable.LAC, TermTable.SlotTimeId, TermTable.BrowsingSession , listToString(TermTable.documents) as documents from TermTable ")
    #base trend Table
    
    final =ee.select("*").groupBy("LAC","SlotTimeId").agg(collect_list("documents"))
    #baseStory.withColumn("re", analiseDocument(baseStory("documents")))
    final.printSchema()
    y = final.select("collect_list(documents)")
    for row in y.collect():
        print(analiseDocument(row))
    
    
    analiseDocument(map(lambda x : x,y))
    #analiseDocument(baseStory.select("documents").collect());
    
    baseStory.show(1000)   
    for words_label in baseStory.select("re").take(90):
        print(words_label) 

    
    # baseTrendTopicsResult.repartition(1).rdd.saveAsTextFile("/TrendTopics/"+str(dataNow)+"/")

 
   # dfRequest.dropTempTable("requests")
   # dfCategories.dropTempTable("Urls");
   # dfLocations.dropTempTable("UserLocation");
    sc.stop()