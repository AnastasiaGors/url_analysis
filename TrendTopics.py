'''
Created on Apr 12, 2016

@author: sestari
'''

from __future__ import print_function
from pyspark import SparkContext,SparkConf
from pyspark.sql import HiveContext
from pyspark.sql.functions import explode,avg,stddev_pop
from dateutil.parser import parse
import time



import os




if __name__ == "__main__":

    os.environ["SPARK_HOME"] = "/Users/sestari/Documents/spark-1.6.1-bin-hadoop2.6"
                                #/usr/hdp/current/spark-client
    
    conf = SparkConf().setAppName("Spark Cluster").set("spark.executor.memory", "1g")
        #setMaster("spark://127.0.0.1:7077")
        #.setSparkHome('/Users/sestari/Documents/spark-1.6.1-bin-hadoop2.6') \
        #.set('spark.executor.extraClassPath', '/Users/sestari/Documents/mongo-hadoop-core-1.5.2.jar') #;/Users/sestari/Documents/mongo-java-driver-3.0.4.jar;/Users/sestari/Documents/mongodb-driver-3.2.2.jar')

    sc = SparkContext(conf=conf) 
        
    sqlContext = HiveContext(sc)
    
    #slot time used by SlideWindow in minutes
    slotTime = 5
    #SlideWindows: used to determine how many slotTime to use
    slideWindows = 4
    halfSlideWindows=2
    #Topics to extract = how many prending topic to extract 
    topicsToRank = 3
    
    dataNow = (parse('2016-04-06 09:45:04'))
  # datetime.strptime('2016-04-06 09:59:04', '%Y-%m-%d %H:%M:%S').date()

        
    def getUniversalSloTime(datep):
        #Unix time (seconds since January 1, 1970)
        timeUnix = time.mktime(datep.timetuple())
        slotTimeSecond = slotTime * 60
        return int(timeUnix // slotTimeSecond)
    #print(getUniversalSloTime(dataNow))
    
    def SlideWindonwExtract(idSlot):
        list = []
        slot = idSlot -1
        countSlot = slideWindows -1
        while (countSlot > 0):           
            list.append(slot)
            slot -= 1
            countSlot -= 1
        result = str(idSlot)
        for p in list:
            result += ", " + str(p)            
        return result
    #print(SlideWindonwExtract(getUniversalSloTime(dataNow)))
    
    def HistoryWindownExtract(idSlot):
        list = []
        slot = idSlot
        for p in range(slideWindows-halfSlideWindows):
            slot -= 1
        result = str(slot)
        slot -= 1
        countSlot = halfSlideWindows -1   
        while (countSlot > 0):
            list.append(slot)
            slot -= 1
            countSlot -= 1
        for p in list:
            result += ", " + str(p)            
        return result
    #print(HistoryWindownExtract(getUniversalSloTime(dataNow)))
        
 
    
    def TrendWindownExtract(idSlot):
        list = []
        slot = idSlot -1
        countSlot = (slideWindows-halfSlideWindows) -1
        while (countSlot > 0):           
            list.append(slot)
            slot -= 1
            countSlot -= 1
        result = str(idSlot)
        for p in list:
            result += ", " + str(p)            
        return result
    #print(TrendWindownExtract(getUniversalSloTime(dataNow)))



    #get json files
    dfRequest = sqlContext.read.json("result1461673574.14/part-00000")
    
    #main table
    dfRequest.registerTempTable("requets")
    #dfRequest.show()
    #dfRequest.printSchema();

    #child table that contais the users Locatins information
    dfLocaBade =  dfRequest.select("GSN","ChargingID","RecordSequence", dfRequest.select("UserLocation")[0])  
    dfLocations = dfLocaBade.select("GSN","ChargingID","RecordSequence",\
                                    dfLocaBade.UserLocation.MCC.alias("MCC"),\
                                    dfLocaBade.UserLocation.MNC.alias("MNC"), \
                                    dfLocaBade.UserLocation.LAC.alias("LAC"),\
                                    dfLocaBade.UserLocation.CID.alias("CID") )                                    
    dfLocations.registerTempTable("UserLocation");
    #dfLocations.show();
    

    
    #table contais the urls informations
    dfCateBase = dfRequest.select("GSN","ChargingID","RecordSequence",explode(dfRequest.select("Urls")[0]))
    dfCategories = dfCateBase.select("GSN","ChargingID","RecordSequence",\
                                    dfCateBase.col.name.alias("name"), \
                                    dfCateBase.col.domain.alias("domain"),\
                                    explode(dfCateBase.col.categories).alias("categories"))  

    dfCategories.registerTempTable("Urls");
    #dfCategories.show();
    

    
    
    
    hql =               "Select UserLocation.LAC,  Urls.categories, requets.SlotTimeId,  COUNT(Urls.categories) as Count from UserLocation \
                            JOIN Urls  ON \
                                Urls.GSN = UserLocation.GSN and  \
                                Urls.ChargingID = UserLocation.ChargingID and \
                                Urls.RecordSequence = UserLocation.RecordSequence \
                            JOIN requets  ON \
                                requets.GSN = Urls.GSN and  \
                                requets.ChargingID = Urls.ChargingID and \
                                requets.RecordSequence = Urls.RecordSequence "                                                     
    hql += "where  requets.SlotTimeId in ( {0} ) ".format(SlideWindonwExtract(getUniversalSloTime(dataNow)))
    # unix_timestamp(requets.RecordOpeningDate)  between unix_timestamp('{0}') and unix_timestamp('{1}') 
    #slot time univoco per non ricercare per data 
    hql += " group By Urls.categories, UserLocation.LAC, requets.SlotTimeId "
    baseStory = sqlContext.sql(hql)
    #base trend Table
    baseStory.registerTempTable("TrendTable")
    #baseStory.show()    



    
    hql_2 = "Select  TrendTable.LAC,  TrendTable.categories, \
                 AVG(TrendTable.Count) as mean, stddev_pop(TrendTable.Count) as variance from TrendTable "
    hql_2 +=   " where  TrendTable.SlotTimeId in ( {0} )  group By TrendTable.LAC, TrendTable.categories Having variance > 0  ".format(HistoryWindownExtract(getUniversalSloTime(dataNow)))
    baseMeanVarianceLocal = sqlContext.sql(hql_2)
    baseMeanVarianceLocal.registerTempTable("MeanVarianceLocal")
    #baseMeanVarianceLocal.show(10000)
    #coalesce(TrendTable.Count, 0)
    #baseMeanVarianceLocal.printSchema()


    hql_2a = "Select  TrendTable.LAC,   \
                 AVG(TrendTable.Count) as mean, stddev_pop(TrendTable.Count) as variance from TrendTable "
    hql_2a +=   " where  TrendTable.SlotTimeId in ( {0} )  group By TrendTable.LAC  Having variance > 0".format(HistoryWindownExtract(getUniversalSloTime(dataNow)))
    baseMeanVarianceGlobal = sqlContext.sql(hql_2a)
    baseMeanVarianceGlobal.registerTempTable("MeanVarianceGlobal")
    #baseMeanVarianceGlobal.show()
    #baseMeanVarianceGlobal.printSchema()

    
    def Zscore(value, mean, variance) :
        if   variance > 0:
            return (value-mean) / variance
        else:
            return 0
            
    sqlContext.registerFunction("Zscore", Zscore)
    
    

    hql_3 = "Select  TrendTable.LAC,  TrendTable.categories, \
                AVG(Zscore(TrendTable.Count, MeanVarianceLocal.mean, MeanVarianceLocal.variance) ) as ZscoreLocal,  \
                AVG(Zscore(TrendTable.Count, MeanVarianceGlobal.mean, MeanVarianceGlobal.variance) ) as ZscoreGlobal \
                from TrendTable \
                JOIN MeanVarianceLocal  ON \
                    TrendTable.LAC = MeanVarianceLocal.LAC and  \
                    TrendTable.categories = MeanVarianceLocal.categories  \
                JOIN MeanVarianceGlobal  ON \
                    TrendTable.LAC = MeanVarianceGlobal.LAC "
                                                                           
    hql_3 +=    " where  TrendTable.SlotTimeId in ( {0} )  group By TrendTable.LAC, TrendTable.categories".format(TrendWindownExtract(getUniversalSloTime(dataNow)))
    # Having ZscoreLocal > 0
    baseTrendTopicsResult = sqlContext.sql(hql_3)
    
    
    #baseTrendTopicsResult = baseTrendTopicsResult.withColumn("Mean", ((baseTrendTopicsResult.ZscoreLocal + baseTrendTopicsResult.ZscoreGlobal) /2))
    baseTrendTopicsResult.registerTempTable("TrendTopic")
    
    #ZscoreLocal , Mean
    hql_e = " SELECT LAC, categories, ZscoreLocal, rank\
            FROM ( \
                SELECT *, ROW_NUMBER() OVER ( \
                    PARTITION BY LAC  ORDER BY ZscoreLocal DESC ) rank FROM TrendTopic \
            ) TrendTopic WHERE rank  <=  "+ str(topicsToRank) + " ORDER BY LAC, rank "
    baseTrendTopicsRanked = sqlContext.sql(hql_e)
    
    
    baseTrendTopicsRanked.show(1000)
    #baseTrendTopicsResult.printSchema()
    
    # baseTrendTopicsResult.repartition(1).rdd.saveAsTextFile("/TrendTopics/"+str(dataNow)+"/")


    
   # dfRequest.dropTempTable("requets")
   # dfCategories.dropTempTable("Urls");
   # dfLocations.dropTempTable("UserLocation");
    sc.stop()
    
