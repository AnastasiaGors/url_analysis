from __future__ import print_function

from pyspark import SparkContext
from pyspark.sql import SQLContext
# $example on$
from pyspark.ml.feature import StopWordsRemover
# $example off$
import os
if __name__ == "__main__":
    
    os.environ["SPARK_HOME"] = "/Users/sestari/Documents/spark-1.6.1-bin-hadoop2.6"
                                #/usr/hdp/current/spark-client
    
   # conf = SparkConf().setAppName("Spark Cluster").set("spark.executor.memory", "1g")
    
    sc = SparkContext(appName="StopWordsRemoverExample")
    sqlContext = SQLContext(sc)

    # $example on$
    sentenceData = sqlContext.createDataFrame([
        (0, ["I", "saw", "the", "red", "baloon"]),
        (1, ["Mary", "had", "a", "little", "lamb"])
    ], ["label", "raw"])

    remover = StopWordsRemover(inputCol="raw", outputCol="filtered")
    remover.transform(sentenceData).show(truncate=False)
    # $example off$

    sc.stop()