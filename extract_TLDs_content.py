from __future__ import with_statement
import os
import sys
import re
import urlparse
from urlparse import urlparse
from BeautifulSoup import BeautifulSoup
import urllib
import urllib2


with open("effective_tld_names.dat.txt") as tld_file:
#     read_data = tld_file.read()
    tlds = [line.strip() for line in tld_file if line[0] not in "/\n"]
def get_domain(url, tlds):
    url_elements = urlparse(url)[1].split('.')
    for i in range(-len(url_elements), 0):
        last_i_elements = url_elements[i:]
        candidate = ".".join(last_i_elements)
        wildcard_candidate = ".".join(["*"] + last_i_elements[1:])
        exception_candidate = "!" + candidate
        if (exception_candidate in tlds):
            return ".".join(url_elements[i:])
        if (candidate in tlds or wildcard_candidate in tlds):
            return ".".join(url_elements[i-1:])
    raise ValueError("Domain not in global list of TLDs")

#print get_domain("http://www.orizzontescuola.it/sites/default/files/imagecache/articolo/images/travel-778338_640_5.jpg", tlds)

#Apply Beautiful Soap to extract words from page:

response = get_domain("http://orario.trenitalia.com/b2c/nppPriceTravelSolutions.do?car=0&stazin=Brescia&stazout=Padova&datag=6&datam=4&dataa=2016&timsh=9&stazin_r=Staz_DA&stazout_r=Staz_A&timsm=46&timsm_r=27&lang=it&nreq=25&channel=tcom&npag=1&lang_r=it&nreq_r=25&channel_r=tcom&npag_r=1&economy=1", tlds)
print response

sep_dom = response.split('.')
print (sep_dom)

def convert(url):
    if url.startswith('http://www.'):
        return 'http://' + url[len('http://www.'):]
    if url.startswith('www.'):
        return 'http://' + url[len('www.'):]
    if not url.startswith('http://'):
        return 'http://' + url
    return url
print convert('trenitalia.com')


# Extract keywords from the content:
page = urllib2.urlopen('http://trenitalia.com').read()
soap = BeautifulSoup(page)
desc = soap.findAll(attrs={"name":"keywords"}) 
print desc[0]['content'].encode('utf-8')





