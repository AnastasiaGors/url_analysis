import re
import nltk
import time
filename = 'entries.txt'

# create list of lower case words with separation by definite tags:
word_list = re.split(r'[/://.%_?=\&__|,\\(\d+)(\n)]+', file(filename).read().lower())
print word_list, '-URL separation'

# remove from the document all words with length<2:
new_string = [s for s in word_list if len(s) > 2]
print new_string, '-remove all with len(words)<2'

# new_string = ' '.join([w for w in word_list if len(w)>2])
# print new_string,

#count words:
print 'Words in text:', len(new_string)

#remove stop words from the document (about, that, the, have....)
word_list2 = [w.strip() for w in new_string if w.strip() not in nltk.corpus.stopwords.words('english')]
print word_list2, '-without stop words'

# create dictionary:frequent pairs
freq_dic = {}

for word in word_list2:
    try: 
        freq_dic[word] += 1
    except: 
        freq_dic[word] = 1
print "sorted by highest freq:"

# create list of (val, key) pairs
freq_list2 = [(val, key) for key, val in freq_dic.items()]
# sort by val or frequency
freq_list2.sort(reverse=True)
freq_list3 = list(freq_list2)
# display result
print freq_list3

for freq, word in freq_list2:
    print word, freq
f = open("wordfreq.txt", "w")
f.write( str(freq_list3) )
f.close()

# def processLanguage():
#     try:
#         for item in word_list2:
#             tokenized = nltk.word_tokenize(item)
#             tagged = nltk.pos_tag(tokenized)
#             print tagged
#             namedEnt = nltk.ne_chunk(tagged)
#             namedEnt.draw()
#             time.sleep(1)
#     except Exception, e:
#         print str(e)
# print processLanguage()

            