'''
Created on Apr 12, 2016

@author: sestari
'''
from __future__ import print_function
from pyspark import SparkContext,SparkConf
from pyspark.sql.types import IntegerType,StructField,StructType,StringType,ArrayType,TimestampType,LongType,Row
from pyspark.sql import HiveContext,SQLContext
from dateutil.parser import parse
from urllib2 import Request, urlopen
import random
import string
import time
import os
##from bs4 import BeautifulSoup
import hashlib
import urllib2

import re
import urllib
import urlparse
from urlparse import urlparse
from macerrors import noMediaHandler
from _ast import TryExcept


if __name__ == "__main__":

    os.environ["SPARK_HOME"] = "/Users/sestari/Documents/spark-1.6.1-bin-hadoop2.6"
    
    os.environ["PYSPARK_SUBMIT_ARGS"]='--master local --executor-memory 2g --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'

    
    conf = SparkConf().setAppName("Clear File")     
    sc = SparkContext(conf=conf)
    sqlContext = HiveContext(sc)

    log = sc.textFile("logs/new.csv").cache()
    
    #slot time used by SlideWindow in minutes
    slotTime = 5
    
    #after open from mongoDBS
    #fieldsCategory = StructType([StructField("PageHash", IntegerType(), False),StructField("values", StringType(), False)]);               
    #department1 = [Row(PageHash=0, values="none")]
    #CategoryDataFrame= sqlContext.createDataFrame(department1)
    
    #CategoryDataFrame.registerAsTable("categories");
    
    
    
    def GenericTranslator(frm='', to='', delete='', keep=None):
        if len(to) == 1:
            to = to * len(frm)
        trans = string.maketrans(frm, to)
        if keep is not None:
            allchars = string.maketrans('', '')
            delete = allchars.translate(allchars, keep.translate(allchars, delete))
        def translate(s):
            return s.translate(trans, delete)
        return translate
    trans = GenericTranslator(frm='-_;.%:/&, ', to='=')
    
    def getCategoriesByURL(value):     
            url = value.lower()
            hashNumberFromUrl = int(hashlib.md5(url).hexdigest(), 16)   
           # valuesCategories = sqlContext.sql("SELECT values  FROM categories WHERE PageHash = "+str(hashNumberFromUrl))
           # print(valuesCategories)
            
            #if valuesCategories.count() == 0:
            print(url)
            worldToSave =''
            try: 
                response = urllib2.urlopen(url).read()                        
                keywords = re.search( "<meta name=\"keywords\".*?content=\"([^\"]*)\"", response)
            except Exception:
                    keywords = None
                    worldToSave =''
            if keywords is not None:
                worldToSave = keywords.group(1)
                #data =[(hashNumberFromUrl, worldToSave.decode('utf-8'))]            
                #a =[Row(PageHash=hashNumberFromUrl, values=worldToSave)]
                #print(a)
                #NewDataFrame= sqlContext.createDataFrame(a)
               #data = sqlContext.sql("select 1 as PageHash , '1' as values")
                #NewDataFrame.write.mode('append').saveAsTable('categories')
                #sqlContext.sql("insert into table categories select t.* from (select 1, '10') t")
                #NewDataFrame.write.insertInto("categories")
                
                #d = NewDataFrame.unionAll(CategoryDataFrame)                                
                
               #CategoryDataFrame.filter("PageHash ="+str(hashNumberFromUrl))
            return worldToSave
                
            #else:
               
               # return valuesCategories.collect()
            
            
                    
 
           # rdd = sc.parallelize(response);            
           # rdd.saveAsTextFile(fileTxt).repartition(1)
            
        
#             
#     getCategoriesByURL("http://globo.com")
#     #getCategoriesByURL("http://globo.com")
#     getCategoriesByURL("http://m.orizzontescuola.it")

    
    def getsplitUrl(x):        
        urlList = x.encode('utf-8').split(', ')
        if len(urlList) > 0:
            key = []
            for url in urlList:
                # remove words that make no sense including attrs
                attrs = ['accessKey=', 'data=', 'cpp=']
                for word in attrs:
                    if word in url:
                        url = url.split(word)[0]
                #url = url.split("accessKey=")[0]
                listUrl = urlparse(url)
                name = listUrl[1]
                ###remove wwww.
                categories = '' #getCategoriesByURL(listUrl[0] +'://'+ name)
                name  = name.replace('www.','')
                
                #get domain
                nameList = name.split('.')
                domain = nameList[-1:]
                rest =  listUrl[4];
                #remove 0123456789&,              
                rest = rest.translate(None, '0123456789')
                
                rest +=' '+categories
                #change from  -_;.%:/ to =
                rest = trans(rest)    
                # insert = space before capital letters
                rest = re.sub('([A-Z])', r'=\1', rest)
                #split by =
                parameters = rest.split('=')
                #get words >3 and <10
                clearParam = filter(lambda c: len(c) >3  and  len(c) < 10, parameters)
                clearParam.append(name)
                clearParam +=nameList[:-1]
                key.append([name,''.join(domain), clearParam])  
        return key


    getsplitUrl("http://www.trenitalia.com/")

  
    
    def getData(datep):
        return parse(datep)
    
    def getUniversalSloTime(datep):
        #Unix time (seconds since January 1, 1970)
        data = parse(datep)
        timeUnix = time.mktime(data.timetuple())
        slotTimeSecond = slotTime * 60
        return int(timeUnix // slotTimeSecond)
    
    print(getUniversalSloTime("2016-04-06 09:32:41.0"))
    

    
    def getExtractLocationAttributes(rATType , location):
        if rATType == 1 or rATType == 2 :
            #222 01 61516 00168 
            #012 34 56789 01234
            arrayLocation = [location[:3],location[3:5],location[5:10],location[10:]]
        else:
            #222 01 6151600 168 
            #012 34 5678901 234
            arrayLocation = [location[:3],location[3:5],location[5:12],location[12:]]
            
        return arrayLocation


    def regularExpressionFilter(url):
        matchObj = re.search('ads.g', url)
        match1bj = re.search('porn', url)
        match2bj = re.search('XXL', url)
        match3bj = re.search('.xvideo', url)        
        match4bj = re.search('fuck', url)
        match5bj = re.search('.jpg', url)
        match6bj = re.search('.xvideos', url)

        if matchObj or match1bj or match2bj or match3bj or match4bj or match5bj or match6bj:
            return False
        else:
            return True

    def urlFilter(httprequest):
        array = []
        for item in httprequest:
           if regularExpressionFilter(item):
                array.append(item) 
        return array
        
  
    
    #get the important fields
    resultMap = log.map(lambda line: line.split(";")).filter(lambda line: len(line)>1). \
                    map(lambda row: (str(row[0]), \
                                     int(row[1]), \
                                     int(row[2]), \
                                     getData(row[3]), \
                                     int(row[4]), \
                                     str(row[5]), \
                                     int(row[6]), \
                                     int(row[7]), \
                                     int(row[8]), \
                                     int(row[9]), \
                                     row[10][1:-1]))
                    
    resultMap_FilterUlr =  resultMap.map(lambda (a,b,c,d,e,f,g,h,i,j,l): (a,b,c,d,e,f,g,h,i,j,urlFilter(l.split(", ")))). \
                            filter(lambda (a,b,c,d,e,f,g,h,i,j,l): len(l) >1)
     
    #put on Json
    fields = StructType( \
                        [StructField("GSN", StringType(), False),  \
                        StructField("ChargingID", IntegerType(), False),  \
                        StructField("RecordSequence", IntegerType(), False),  \
                        StructField("RecordOpeningDate", TimestampType(), False),  \
                        StructField("rATType", IntegerType(), False),  \
                        StructField("UserLocation", StringType(), False),  \
                        StructField("Accuracy", IntegerType(), False),  \
                        StructField("BrowsingSession", IntegerType(), False),  \
                        StructField("Uplink", IntegerType(), False),  \
                        StructField("Downlink", IntegerType(), False), \
                        StructField("Urls", ArrayType(StringType(),False))])
    
     #The new Json Format
    newStructure = StructType( \
                        [StructField("GSN", StringType(), False),  \
                        StructField("ChargingID", StringType(), False),  \
                        StructField("RecordSequence", IntegerType(), False),  \
                        StructField("RecordOpeningDate", TimestampType(), False),  \
                        StructField("rATType", IntegerType(), False),  \
                        StructField("SlotTimeId", LongType(), False),  \
                        StructField("UserLocation", StructType([ \
                                                                StructField("MCC", StringType(), False), \
                                                                StructField("MNC", StringType(), False), \
                                                                StructField("LAC", StringType(), False), \
                                                                StructField("CID", StringType(), False), \
                                                                ]), False),  \
                        StructField("Accuracy", IntegerType(), False),  \
                        StructField("BrowsingSession", IntegerType(), False),  \
                        StructField("Uplink", IntegerType(), False),  \
                        StructField("Downlink", IntegerType(), False), \
                        StructField("Urls",ArrayType( \
                                                     StructType([StructField("name", StringType(), False),StructField("domain", StringType(), True), \
                                                                    StructField("categories", ArrayType(StringType(), True), True)]), False), \
                                                True)])
            
        #get the important fields
    resultMap_newFormat = log.map(lambda line: line.split(";")).filter(lambda line: len(line)>1). \
                                 map(lambda row: (str(row[0]), \
                                     int(row[1]), \
                                     int(row[2]), \
                                     getData(row[3]), \
                                     int(row[4]), \
                                     getUniversalSloTime(row[3]), \
                                     getExtractLocationAttributes(row[4],row[5]), \
                                     int(row[6]), \
                                     int(row[7]), \
                                     int(row[8]), \
                                     int(row[9]), \
                                     getsplitUrl(row[10][1:-1])))
                    
     
    schemaDataFrame= sqlContext.applySchema(resultMap_newFormat, newStructure)
    
    schemaDataFrame.repartition(1).write.format('com.databricks.spark.csv').save("preprocess/"+str(time.time()))

    
 
    sc.stop()
    
